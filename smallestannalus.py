import numpy as np 
import pylab as pl
import matplotlib.pyplot as plt
from shapely.geometry import LineString
from scipy.spatial import Voronoi, voronoi_plot_2d, ConvexHull

def smallestann(sSet):
	"""
	sSet: support set output from ransac
	"""
	def euclDistance(a, b):
		return np.sqrt((a[0]-b[0])**2+(a[1]-b[1])**2) 
	

	def radiusInCirle(points, center):

		sqd = np.sqrt(((points - center)**2).sum(axis=1))
		idx =np.argsort(sqd)
 		
		return euclDistance(points[idx[0]], center)  

	def swidthAnnalus(candAnnuli):
		"""
		candAnnali: candidates annali 
		"""
		swidth = -1
		pi = 3.14

		for ca in candAnnuli:
			if swidth == -1:
				swidth = (ca['rcinner']**2 - ca['rcouter']**2)*pi
			else: 
				width = (ca['rcinner']**2 - ca['rcouter']**2)*pi	
				if width < swidth:
					swith = width
					sinn = ca['rcinner']
					sout = ca['rcouter']
					center = ca['center']

		return rcouter, rcinner, center 

	def parametersAnnalus(couter, cinner, center, supportSet):
		"""
			couter, cinner: distances from center 
			supportSet: output from ransac
		"""
		pi = 3.14
		n = 0 
		for ss in supportSet:
			if (euclDistance(ss, center) <= couter) and (euclDistance(ss, center) >= cinner):
				n = n + 1
								
		w = cinner
		e = couter - cinner
		a = (couter**2 - cinner**2)*pi
		p = a / n 
		qn = (n*(e+n-1)*(2*pi)**(n-1)*(w**2)**(n-1)*e**(n-2))/a**(n-1)
		en = (2*pi)**(n-1)/(np.math.factorial(n-2))*(p**n)*(w**(2*n-2))*(e**(n-2))*a

		return qn, en 

	# First case: compute VD -  determine farthest points
	chull = ConvexHull(sSet)
	vd = Voronoi(sSet)
	candAnnuli = []
	candCenters = vd.vertices
		
	for cc in candCenters:
		farDist = 0
		dist = 0

		for cv in sSet[chull.vertices]:
			dist = euclDistance(cc, cv)
			if dist >= farDist:
				farDist = dist 
		
		rcinner = radiusInCirle(sSet, cc) 
		rcouter = farDist
		candAnnuli.append({'center': cc, 'rcinner': rcinner, 'rcouter': rcouter}) 
	
	# Second case: compute 	FPVD - determine closest point
	fpvd = Voronoi(sSet[chull.vertices], furthest_site = True)
	ccandCenters = fpvd.vertices

	for cc in ccandCenters:
		sqd = np.sqrt(((sSet - cc)**2).sum(axis=1))
		idx =np.argsort(sqd)
		rccinner = euclDistance(sSet[idx[0]], cc)
		rccouter = euclDistance(sSet[idx[len(idx)-1]], cc)
		candAnnuli.append({'center': cc, 'rcinner': rccinner, 'rcouter': rccouter})  
	
	# Third case: compute intersections of the VD and FPVD vertices 
	# ridge vertices: indeces of the voronoi vertices forming each voronoi edges!!!
	# -1 indicates not in voronoi diagram

	for v in vd.ridge_vertices:
		
		if (v[0] != -1) and (v[1]!= -1):
			va = candCenters[v[0]]
			vb = candCenters[v[1]]       
			
			for f in fpvd.ridge_vertices:
				
				if (f[0] != -1) and (f[1]!= -1):
					fa = ccandCenters[f[0]]
					fb = ccandCenters[f[1]]
					vdseg = LineString([va, vb])
					fpseg = LineString([fa, fb])
					ints = vdseg.intersection(fpseg) 
					
					if(ints):
						candAnnuli.append({'center': [ints.x, ints.y], 'rcinner':euclDistance(va, vb), 'rcouter':euclDistance(fa, fb)})
					
	ann = swidthAnnalus(candAnnuli)
	
	return parametersAnnalus(candAnnuli[0]['rcouter'], candAnnuli[0]['rcinner'], candAnnuli[0]['center'], sSet)
		

if __name__ == '__main__':
	from ransac import ransac
	from points import getPoints

	points = np.array(getPoints("data.png")).T
	supportSet = ransac(points, 100, 15, True) 
	print(smallestann(supportSet))
	
