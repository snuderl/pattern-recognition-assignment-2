import numpy as np
from scipy.ndimage import imread
from scipy.ndimage.morphology import binary_erosion, binary_hit_or_miss

def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.299, 0.587, 0.144])


""" Return 2 arrays containing x and y coordinates of points
	([1,5, 8], [1, 7, 8])
"""
def getPoints(image, display=False):
	im = rgb2gray(imread(image))

	black = np.nonzero(im < 50)

	newImage = np.zeros(im.shape)
	newImage[black] = 1

	points = binary_erosion(newImage, iterations=1)

	if display:
		from matplotlib import pyplot as plt
		import matplotlib.cm as cm
		plt.imshow(points, cmap = cm.Greys_r)
		plt.show()

	return np.nonzero(points)


if __name__ == "__main__":

	getPoints("data2.png", True)