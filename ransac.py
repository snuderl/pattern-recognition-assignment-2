from numpy.random import choice
import numpy as np

def norm(x):
	return x[0] * x[0] + x[1] * x[1]

""" Equation rom wikipedia on https://en.wikipedia.org/wiki/Circumscribed_circle
	Returns coordinates of the center and its radius
"""
def generateCircle(a, b, c):
	ax, ay = a
	bx, by = b
	cx, cy = c
	d = 2 * ax * (by - cy) + bx * (cy - ay) + cx * (ay - by)

	#print norm(a), norm(b), norm(c)
	centerx = norm(a) * (by - cy) + norm(b) * (cy - ay) + norm(c) * (ay - by)
	centery = norm(a) * (cx - bx) + norm(b) * (ax - cx) + norm(c) * (bx - ax)

	center = np.array([centerx, centery]) / d

	radius = norm(center - a) ** 0.5

	return center, radius


def error(center, r, points):
	cp = points - center ## centered points
	return np.abs((cp[:, 0] ** 2 + cp[:, 1] ** 2) ** 0.5 - r)



def ransac(points, iters, maxDistance):
	"""
	points :: n x 2 array of numbers
	iters  :: number of iterations of randsac algorithm
	maxDistance :: threshold distance to determine if good fit

	return :: tuple where first value is the center of circle and radius
	"""
	best = None
	bestScore = -1

	for _ in range(iters):
		indices = choice(range(0, len(points)), 3)
		params = points[indices, :]
		center, r = generateCircle(*params)

		### I think we need to replace error and score with the voronoi stuff 
		### and calculate covering prob.
		errors = error(center, r, points)
		score = np.sum(errors < maxDistance)

		if score > bestScore:
			bestScore = score
			best = center, r

	return best



if __name__ == "__main__":
	from points import getPoints

	points = np.array(getPoints("data.png")).T ### n * 2 array
	print (ransac(points, 100, 15))

